import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';


const Buttons = () => {
    const useStyles = makeStyles((theme) => ({
        root: {
          '& > *': {
            margin: theme.spacing(1),
          },
        },
      }));
       
    return (
        <div>
             <Button variant="contained" color="primary">
             Confirmer
      </Button>
        </div>
    )
}

export default Buttons
