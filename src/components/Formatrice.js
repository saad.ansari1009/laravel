import React from 'react'
import {useState,useEffect} from 'react';
import MaterialTable from 'material-table';
import axios from 'axios';
const Formatrice = () => {
    const [B, setB] =new useState([]);
    const [C,setC]=new useState([]);
    useEffect(async () => {
        Adding();
        const D = fetch("http://127.0.0.1:8000/api/t/c")
        D.then(e => e.json()).then(e => Array(setC(e)));
    }, [])
      const data=B.map(e=>e)

      const columns=[
          {
            title:'IDFormation',field:'IdF'
          },
          {
            title:'Libellé',field:'Libellé'
          },
          {
            title:'DateDebut' ,field:'DateDebut'
          },
          {
            title:'DateFin' , field:'DateFin'
          }
      ]
     const AddFormation=()=>{
       var Libellé=document.getElementById('Libellé');
       var DateDebut=document.getElementById('DateDebut');
       var DateFin=document.getElementById('DateFin');
       var IdFormatrice=document.getElementById('Formatrice');
       var IdType=document.getElementById('TypeFormation');
      axios({
        method: "post",
        url:"http://127.0.0.1:8000/api/t/b",
         data:{Libellé:Libellé.value,DateDebut:DateDebut.value,DateFin:DateFin.value,Id:IdFormatrice.value,IDT:IdType.value},
        headers: { "Content-Type": "application/json" },
      }).then(e=>{
        Adding();
      })
      
     }
     const Adding=()=>{
        const A = fetch("http://127.0.0.1:8000/api/t/a")
        A.then(e => e.json())
        .then(e =>{
            setB(e)
            console.log("done");
        });
        
     }
    return (
        <div>
          <div>
          <MaterialTable title='Liste des formations' data={data} columns={columns}/>
          </div>
          <button onClick={AddFormation}>Ajouter</button>
           <div className='cardStyle'>
             <form className='Formulaire'>
                Libellé: <input type='text'id='Libellé'/> <br/>
                DateDebut:<input type='text'id='DateDebut'/><br/>
                  DateFin:<input type='text'id='DateFin'/> <br/>
                  Id:<input type='text'id='Formatrice'/>  <br/>
                  {/* IdType:<input type='text' id='TypeFormation'/> */}
                  TypeFormation: <select  id='TypeFormation'>{C.map((e)=>(<option key={e.IDT} value={e.IDT} >{e.Type}</option>))}</select>
             </form>
           </div>
        </div>
    )
}

export default Formatrice
